#!/usr/bin/env node
// Copyright © 2023 Haelwenn (lanodan) Monnier <contact+kanaconv@hacktivis.me>
// SPDX-License-Identifier: MIT

const {argv, stdin, stdout} = require('node:process');
const readline = require('node:readline');
const rl = readline.createInterface({input : stdin, output : stdout});

const Kuroshiro = require('kuroshiro');
const KuromojiAnalyzer = require('kuroshiro-analyzer-mecab');
const kuroshiro = new Kuroshiro();

let args = require('minimist')(argv.slice(2));

let mode = args['mode'] || args['m'] || 'furigana';
let to   = args['to'] || args['t'] || 'romaji';

kuroshiro.init(new KuromojiAnalyzer());

function
convert(line)
{
	kuroshiro.convert(
		line,
		{mode: mode, to: to}
	).then((res) => {
		console.log(res);
	});
};

rl.on('line', convert);

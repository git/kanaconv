# kanaconv: Command to convert Japanese from/to Kana/Kanji/Romaji with furigana option

I created this tool as I wanted to have a way to easily generate [furigana](https://en.wikipedia.org/wiki/Furigana) from Japanese text, a feature missing from [kakasi](http://kakasi.namazu.org/), which I'd recommend also checking out.

## Dependencies
- A CommonJS engine, such as NodeJS
- [kuroshiro](https://kuroshiro.org/) `^1.2.0`
- [kuroshiro-analyzer-mecab](https://github.com/hexenq/kuroshiro-analyzer-mecab) `^1.0.0`
- [minimist](https://github.com/minimistjs/minimist) `^1.2.8`

Mecab Analyzer was picked as Mecab is widly packaged in distros while the rest are NPM-only and rather obscure by comparison.  
Should also be noted that the only indirect hard-dependencies are mecab-async and shell-quote, this makes this software packageable in distributions the traditional way.

## Copyright
```
Copyright © 2023 Haelwenn (lanodan) Monnier <contact+kanaconv@hacktivis.me>
SPDX-License-Identifier: MIT
```
